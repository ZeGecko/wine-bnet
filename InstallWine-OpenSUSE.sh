#!/bin/bash
#
# Script to install Wine and the dependencies Battle.net may need.
# Version: 1.0
#
# Make this script executable with chmod 755
# or under Permissions in Properties dialogue.
#
# https://wiki.winehq.org/Download



shopt -s nocasematch

# Check if script is running with root privliages.
if [[ $EUID -ne 0 ]]; then
    echo "Error: Root privliages are required!" >&2
    exit 1
fi

# Check if OS is openSUSE.
if [[ $(lsb_release -i | awk '{ print $3 }') != "openSUSE" ]]; then
    echo "Error: OS not detected as openSUSE." >&2
    exit 1
fi

# Add Wine repository if not Tumbleweed.
release="$(lsb_release -d | awk -F'[:][ \t]*' '{ print $2 }' | tr ' ' '_')"
if [[ $release != "openSUSE_Tumbleweed" ]]; then
    # Tumbleweed repository is missing 32bit dependencies, standard Tumbleweed repo is current enough.
    echo "Adding Wine's repository..."
    zypper ar -cfp 90 http://download.opensuse.org/repositories/Emulators:/Wine/$release/ 'Emulators:Wine'
    zypper dup --from 'Emulators:Wine' --allow-vendor-change
fi



# Preform installation of Wine and various dependencies.
echo "Installing Wine..."
zypper -n in wine-staging

echo "Installing dependencies..."
zypper -n in winetricks
zypper -n in cabextract
zypper -n in libgnutls-devel
zypper -n in libgnutlsxx28
zypper -n in libgnutls30
zypper -n in libgpg-error0
