Some simple bash shell scripts for installing Battle.net under Wine.

For some reason I struggled getting Lutris to work, so these are simple scripts
to automate the process of setting up a prefix for Battle.net and its games
under Wine called Battle.net. Games themselves should be installed into 
a seperate folder outside of the prefix, and configure scripts by default
will set up a Games folder, and place a symbolic link within the Battle.net
Wine prefix pointing to them so the launcher will not know the difference.

Look to Lutris for dependencies you should install:
https://github.com/lutris/lutris/wiki/Game:-Blizzard-App

In addition you will need Winetricks.

InstallWine-OpenSUSE.sh InstallWine-MintUbuntu.sh:
Scripts for openSUSE and Linux Mint / Ubuntu to add repository if necessary,
then download and install Wine and any missing dependencies.
Note: These will not handle any dependencies / drivers for your GPU.

InstallBattleNet.sh:
Script to download the Battle.net desktop app, create a Wine prefix named Battle.net,
and use Winetricks to install corefonts and DXVK.

ConfigureHearthstone.sh ConfigureHotS.sh ConfigureWoW.sh:
Simple scripts to create a symbolic link to game folders in ~/Games.
You should run these after running InstallBattleNet.sh,
and can easily modify to point to whatever folder a game is installed in.
