#!/bin/bash
#
# Script to install Battle.net under its own prefix in Wine.
#
# Make this script executable with chmod 755
# or under Permissions in Properties dialogue.
#
#Location to install wine prefix to on user's home directory:
winePrefixLocation=".WineCellar"



# Download setup file if not in current directory.
downloadBattleNet() {
    if [ ! -f "./Battle.net-Setup.exe" ]
    then
        # Get system's locale and make sure it is one Blizzard offers a download for, otherwise default to enUS.
        local currentLocale="$(locale | grep 'LANG=' | awk -F '[=_.]' '{ print $2 $3 }')"
        if [ $currentLocale != "enUS" ] && [ $currentLocale != "enGB" ] && [ $currentLocale != "esMX" ] \
                && [ $currentLocale != "esES" ] && [ $currentLocale != "deDE" ] && [ $currentLocale != "frFR" ] \
                && [ $currentLocale != "itIT" ] && [ $currentLocale != "koKR" ] && [ $currentLocale != "ptBR" ] \
                && [ $currentLocale != "ptPT" ] && [ $currentLocale != "plPL" ] && [ $currentLocale != "ruRU" ] \
                && [ $currentLocale != "zhTW" ] && [ $currentLocale != "zhCN" ]
        then
            currentLocale="enUS"
        fi
        
        echo
        echo "Downloading the Blizzard Battle.net Desktop App..."
        echo
        wget -O ./Battle.net-Setup.exe "https://www.battle.net/download/getInstallerForGame?os=win&locale=$currentLocale&version=LIVE&gameProgram=BATTLENET_APP"
    fi
}

# Create prefix and install necessary components to run Battle.net and its games.
installWinetricksComponents() {
    # Create prefix directory in user's home directory if one doesn't exist.
    echo
    echo "Battle.net wine prefix will be stored in /home/$currentUser/$winePrefixLocation/Battle.net"
    [ ! -d "/home/$currentUser/$winePrefixLocation" ] && mkdir -p "/home/$currentUser/$winePrefixLocation"
    
    # Install corefonts and dxvk.
    echo "Answer yes to any dialog boxes which may open!"
    echo
    echo "Installing core fonts..."
    env WINEPREFIX="/home/$currentUser/$winePrefixLocation/Battle.net" winetricks corefonts
    echo
    echo "Installing Vulkan support (DXVK)..."
    env WINEPREFIX="/home/$currentUser/$winePrefixLocation/Battle.net" winetricks dxvk
}

# Preform installation of Battle.net.
installBattleNet() {
    # Wait for all processes to finish (if download is still running).
    wait
    
    # Install Battle.net into our prefix.
    clear
    echo "Now installing Battle.net."
    echo "For best results, please do not log in after installation is complete."
    echo "Close the log in window and this console, then launch Battle.net from your desktop."
    sleep 3
    echo
    echo "Installing Battle.net..."
    env WINEPREFIX="/home/$currentUser/$winePrefixLocation/Battle.net" wine Battle.net-Setup.exe
}

# Wait for Battle.net installer to finish then end script when launcher starts.
bnetKiller() {
    sleep 30
    local setupPid=$(pidof Battle.net-Setup.exe)
    while [ -e /proc/$setupPid ]
    do
        sleep 1
    done
    # wait $(pidof Battle.net-Setup.exe)
    
    local launcherPid=$(pidof Battle.net.exe)
    until [ -e /proc/$launcherPid ]
    do
        sleep 1
    done
    kill $(pidof Battle.net.exe)
    exit
}



clear

# Get current user's login name.
currentUser=$(whoami)

# Check if script is running with root privliages.
if [ "$currentUser" == "root" ]
then
    echo "Error: Please do not run this script with root privliages!" >&2
    exit 1
fi

# Check for missing dependencies then run script.
if ! type wine > /dev/null || ! type winetricks > /dev/null
then
    echo "Error: Wine and/or Winetricks is not installed!" >&2
    exit 1
elif ! type cabextract > /dev/null
then
    echo "Error: cabextract is not installed!" >&2
    exit 1
else
    downloadBattleNet &
    installWinetricksComponents
    
    installBattleNet &
    bnetKiller
fi
