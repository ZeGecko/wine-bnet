#!/bin/bash
#
# Script to install Wine and the dependencies Battle.net may need.
# Version: 1.0
#
# Make this script executable with chmod 755
# or under Permissions in Properties dialogue.
#
# https://wiki.winehq.org/Ubuntu



shopt -s nocasematch

# Check if script is running with root privliages.
if [[ $EUID -ne 0 ]]; then
    echo "Error: Root privliages are required!" >&2
    exit 1
fi

# Check if OS is Mint or Ubuntu.
dist="$(lsb_release -i | awk '{ print $3 }')"
if [[ $dist != "LinuxMint" && $dist != "Ubuntu" ]]; then
    echo "Error: OS not detected as either Linux Mint or Ubuntu." >&2
    exit 1
fi

# Get this release's Ubuntu codename.
if [[ $dist == "Ubuntu" ]]; then
    codeName="$(lsb_release -c | awk '{ print $2 }')"
else
    codeName="$(cat /etc/os-release | grep '^UBUNTU_CODENAME=' | awk -F\= '{ print $2 }')"
fi

# Enable 32bit architecture on 64 bit systems.
if [[ $(uname -m) == "x86_64" ]]; then
    echo "Enabling 32bit architecture..."
    dpkg --add-architecture i386
fi



# Preform installation of Wine and various dependencies.
echo "Adding Wine repository..."
wget -q -O - https://dl.winehq.org/wine-builds/winehq.key | apt-key add -
rm -f winehq.key
apt-add-repository "deb https://dl.winehq.org/wine-builds/ubuntu/ $codeName main"
apt-get update

echo "Installing Wine..."
aptitude -f install winehq-staging -y

echo "Installing dependencies..."
apt-get install winetricks cabextract libgnutls30:i386 libldap-2.4.2:i386 libgpg-error0:i386 libsqlite3-0:i386 -y
