#!/bin/bash
#
# Script to prepare Battle.net prefix for WoW.
#
# Make this script executable with chmod 755
# or under Permissions in Properties dialogue.
#
# Note: Run this script after Battle.net wine prefix
#       has been created but before logging in.
#       
#
# Wine prefix and game install locations on home directory:
winePrefixLocation=".WineCellar"
gameInstallLocation="Games"

# Battle.net wine prefix:
bnetPrefix="Battle.net"



######### Do not edit below this line! ####################

clear

# Get current user's login name.
currentUser=$(whoami)

# Internal variables. Do not change.
gameFolder="World of Warcraft"
gameDirectory="/home/$currentUser/$gameInstallLocation/$gameFolder"
winRoot="/home/$currentUser/$winePrefixLocation/$bnetPrefix/drive_c"

# Check if script is running with root privliages.
if [ "$currentUser" == "root" ]; then
    echo "Error: Please do not run this script with root privliages!" >&2
    exit 1
fi

# Create game directory if it does not exist.
if [ ! -d "$gameDirectory" ]; then
    mkdir -p "$gameDirectory"
fi

# Create symbolic link to game folder in Battle.net prefix.
# We store the games outside of the prefix so if we ever need to recreate the prefix,
# the games files won't be affected.
if [ ! -L "$winRoot/Program Files (x86)/$gameFolder" ] && [ ! -d "$winRoot/Program Files (x86)/$gameFolder" ]; then
    echo "Creating symbolic link to $gameFolder in Wine prefix..."
    ln -s "$gameDirectory" "$winRoot/Program Files (x86)/$gameFolder"
fi

echo "Done."
